---
layout: post
title: Second interview
---

I've been meeting up with my [supervisor](http://jacek.soc.port.ac.uk) over the past couple of weeks, mainly discussing how the artefact of the project is going to work, as opposed to talking about the literature review.

Last week, we talked about tracking attendance using Bluetooth as well as RFID and NFC. We discussed the scientific value of the paper and how it could be improved by anonymising the data of students that's being kept in the system after they leave university. Unfortunately, due to the fact that there's very little to no marks granted for this, I don't think it's a wise idea to prioritise this over the rest of the work that has to be done. Instead, I've decided to
look into it later, if there's time.

I held a requirements gathering interview with Jacek on Friday. I feel like I managed to get a clearer view on the project and what Jacek was expecting from it. Here's the main points of the interview:

* **Face time is important**: The thing that seemed to come up more than anything was this. Face time with students is very important to Jacek and seems to be one of his primary requirements when it comes to the project. As long as the artefact can maintain a good balance of face time while still making things faster for both the lecturer and the student, then it's a success.
* **In-app student history and stats**: The lecturer should be able to have an option to see a student's attendance history and stats in a couple taps. This can be useful in times where a lecturer doesn't recognise a student. In that case, they can simply tap on their history to find out if the reason they don't recognise them is because they haven't been attending lately, or because something else is going on.
* **Web application fallback**: In the likely possibility that a lecturer's smart device runs out of juice, Jacek voiced his opinion on having a web application fallback which he could access from any device, including the university PCs that are provided in each lecture and practical room. This web application would allow the lecturer to add students to the register using manual input, similar to how the native application would be able to, albeit without NFC or Bluetooth.
* **In-depth web application history and stats**: The web application should also include more in-depth data of a student's attendance history and stats, possibly including graphs and charts. The native application should link to this part of the web application when a lecturer is using the native application to view a student's history.
* **Adding/removing students**: Simply put, the lecturer should be able to add or remove students that have transferred or cancelled their studies, thereby improving the data that is output by the artefact in total, and making it easier for the lecturer to keep track of attendance.
* **Importing is low priority**: Importing student data from spreadsheets or other means is of low priority to Jacek. If other interviewees decide that this is low priority, this can be removed from the plans, making it much easier for me to implement, while still giving the people what they want.
* **Finally, speed**: The application must, at all times, be faster than the alternative of using spreadsheets. Paper can't really be beat on this, but the application must be faster than the other alternatives.

This concludes the second interview. I've e-mailed a couple more lecturers asking them for an interview; hopefully I'll hear back from them soon!

~R
