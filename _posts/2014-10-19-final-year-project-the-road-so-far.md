---
layout: post
title: Final Year Project&#58; The Road So Far
---

As part of my final year at the University of Portsmouth, I've been tasked with carrying out a final year project that utilises the skills I've learned during my course as well as my areas of interest within computer science.

I initially planned on creating an educational coding game in the form of an MMORPG for my final year project. However, after a few weeks of research, it became clear that this project was too ambitious and would be too much to handle in addition with the rest of my units.

I started looking into ideas that various lecturers had suggested on their websites as potential final year project material, and that's when I stumbled upon one of [Jacek Kopecky](http://jacek.soc.port.ac.uk)'s ideas; a student attendance monitoring system. As I had worked in [Information Services](http://www.port.ac.uk/special/is/) for my placement year, I was really interested in anything university-related, and the idea got my attention pretty fast.

I did a bit of research on the matter and had a think about how I could implement this as a final year project, and then  contacted Jacek and set up a meeting with him. The meeting went great and Jacek was happy to supervise me, despite the fact that another student had already contacted him about being supervised on the same subject.

I've had a few meetings with Jacek since, and he's already given me some very solid advice on how I could approach the subject, without spoiling all the fun and still letting me do a lot of work. I must say I am quite glad I chose Jacek to be my supervisor for this project; I don't think this would have gone so well with any other lecturer.

So, after a lot of research, I proposed a more extensive version of Jacek's initial idea: an NFC-Assisted Attendance Monitoring application for Android. The students would go to the lecturer's desk as soon as they entered the lecture theatre and swipe their cards on the lecturer's phone. The application will automatically figure out which student the card belongs to and add them to the attendance register, thus significantly reducing the time that is required for
taking student attendance.

I recently finished the [Project Initiation Document](http://goo.gl/Kvwz9c) for the project and made a [GitHub Repository](http://goo.gl/Fnern7) that contains the code for the prototype. I'm meeting up with Jacek again on Tuesday in order to hand-in my PID and talk about the Literature Review part of the Project Report.

~R
