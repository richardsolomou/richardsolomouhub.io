---
layout: post
title: Wealth of Resources
---

Today I had a meeting with [Jacek](http://jacek.soc.port.ac.uk), my project supervisor, in which I handed in the final printed version of my [Project Initiation Document](http://goo.gl/Kvwz9c). With my PID submitted, it is now time to start writing the Literature Review!

Over the past week, I had looked into research papers that explained ways in which other universities have approached the attendance monitoring problem using NFC and RFID. Through [Google Scholar](http://scholar.google.com) and [Engineering Village](http://www.engineeringvillage.com), I managed to find 25 relevant papers on the subject which seemed very promising. I had skimmed through all of them and annotated most, finding things that they did that were similar to my approach, as well as things that they did that were in a completely different direction.

After handing in my PID, the first thing Jacek and I talked about was research papers. I showed him the papers I found and we went through a couple of them, focusing on how I could write about them in my literature review. There were, in fact, a couple paid papers that I couldn't get my hands on. Jacek suggested I find the links to those papers, as there could be a way to get them for free. Furthermore, Jacek mentioned that some of these papers seem repetitive, and that I
may end up using only a handful of them.

Jacek was curious as to why I was keen on getting papers that talked about NFC/RFID. He went on by saying that I should have a look at what other ways people came up with solving the attendance monitoring problem that don't have to do with RFID, why they thought it was a problem, and why they thought it was a good solution. Jacek mentioned that I'll probably be able to get these answers by broadening my paper resource topic.

On the software side, we talked about extending the functionality of the artefact so that it could be used by multiple lecturers and for multiple units. As I am planning on releasing the software to the public after it's completed, this could prove to be very useful.

Final thoughts and things to look into for next week:

* Broaden research paper topic for non-RFID solutions.
* Structure literature review by points made in research papers.
* Handle multiple lecturers using [Google+ Sign In](http://developer.android.com/google/play-services/plus.html) for staff accounts.
* Restructure database for multiple lecturers and multiple units.
* Import students into database from current and existing Google Spreadsheet.

~R
