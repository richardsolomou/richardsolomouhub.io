---
layout: post
title: Initial commit
---

I originally registered [rsolomou.com](http://www.rsolomou.com) back in 2011, but never got around to adding any content to it. In early 2014, I registered [richardsolomou.com](http://www.richardsolomou.com) and started working on that. I was initially planning on setting up a WordPress blog to go with it but, alas, I never really got around to it.

Around March of 2014, I found out about Jekyll, a simple, blog-aware, static site generator. Jekyll just so happened to be the engine behind GitHub Pages, which meant I could use it for hosting my website on GitHub's servers for free.

Soon thereafter, I began pushing commit after commit using Jekyll as the foundation of my website, finally getting to the point of publishing my work on [richardsolomou.com](http://www.richardsolomou.com). However, even though Jekyll was a "blog-aware static site generator", I never got around to making a blog out of it. *Until now*.

After eight months of originally buying this domain name and about six months since the beginning of the development for this website, I finally got around to setting up my blog. This is what you should expect to see from this blog:

* General content and anything that has to do with this website.
* Entries about university and my final year project, studies, coursework, etc.
* Posts regarding real life, seen from an MMORPG player's perspective.
* Anything even mildly interesting to me.

Hopefully I won't be that person that starts something and stops posting after a couple of weeks. Let's see how it goes!

~R
