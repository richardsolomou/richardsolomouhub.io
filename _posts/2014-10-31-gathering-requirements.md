---
layout: post
title: Gathering requirements
---

I saw my project supervisor on Tuesday and we talked about a few of things, most important of which was the gathering of requirements. The first thing we talked about was getting multiple lecturers be able to sign in to the application using their device accounts, thus needing no password to login. After a couple days of tinkering around with this, the [prototype](https://github.com/richardsolomou/atmos-hybrid-app) now has this functionality and stores the user data in a database on the device.

The next thing on the agenda was to look at literature that discussed ways of solving the attendance problem without NFC or RFID. I found a few papers that went into this, but nothing too interesting or ground-breaking. One of the solutions was to take a picture of the student every time they entered the classroom. Jacek suggested that this solution could be improved by taking a picture of the entire classroom and running facial recognition to get all present students. Jacek suggested that I should look into older literature that discussed the benefits and drawbacks of attendance monitoring systems, as well as look into [what happened a couple of years ago](http://www.bbc.co.uk/news/education-25152582) at the London Metropolitan University when its license was revoked after investigations by the UK Border Agency which suggested that student attendance was not being monitored and many international students had no right to be there.

Then, we talked about gathering requirements. This was *such* an enlightening and interesting discussion. Jacek initially mentioned getting ideas and evidence from what the CAM Office is currently doing in order to monitor student attendance. Furthermore, we talked about possibly handing out questionnaires to lecturers, who would in turn provide valuable feedback to figure out the project's requirements. We quickly realised that this was not the best approach, and we decided it was best to hold interviews with the lecturers instead. These interviews would have to be managed very well, both in terms of time and not going off-track. Additionally, they could also be recorded so that I can always go back on them in case I missed something.

In these interviews I plan on getting information from lecturers on what they're currently doing in order to monitor attendance, what they're doing differently from other lecturers, how they'd prefer to be doing things, and what other features they could suggest for the project.

Finally, Jacek and I talked about note-taking while I'm working on the prototype. I mentioned that I'm already currently blogging and that I'm pushing commits to GitHub for everything I change on the prototype. Jacek mentioned that if I can correctly document the use of my blog and the commits and issues on GitHub, I could get an immense amount of marks in the project management section of the marking scheme, but only as long as I do it right.

Final thoughts and things to look into for next week:

* Get a list of questions that I would like to ask lecturers during interviews.
* Start sending out the first batch of e-mails to lecturers, requesting half an hour to about an hour of interview time.
* Start thinking about the literature review structure and how I can broaden it away from technology.

~R
