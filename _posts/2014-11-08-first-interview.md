---
layout: post
title: First interview
---

I met up with my supervisor on Tuesday and we talked about requirements gathering through interviews with lecturers. Earlier, I had written some questions and filled out the agenda for the interviews I would be conducting, so I had some things to show him and get feedback on before the actual interviews began.

[Jacek](http://jacek.soc.port.ac.uk) and I talked a bit about integrating my final year project with [PUMS](http://www.pums.cam.port.ac.uk/), the system that is currently in use for tracking what project each final year student is doing. Currently, project supervisors have to make a note of whenever they have a meeting with a student, otherwise the student is marked as absent. The idea was to allow final year students to tap on a lecturer's phone to be added to the register for PUMS, therefore reducing the amount of time required for making sure that students attend timetabled events. After my meeting with Jacek was over, I sent an e-mail over to [Rich Boakes](http://rjb.soc.port.ac.uk/) requesting a meeting to hold an interview.

On Friday, I met up with Rich to discuss my final year project and get his thoughts on it. The interview went quite smoothly and it was very interesting to get a different point of view on the project. This is a short overview of what took place, and how everything went:

* **Paper is annoying**: Rich initially talked about the hassle of having to carry around (and remember to carry around) a bunch of papers that the CAM Office would give him at the beginning of the year, which he has to fill out in order to monitor attendance. It would be possible for the application to send out attendance data to the CAM Office every so often so that the lecturer wouldn't have to do it manually.
* **Attendance Gamification**: Students would be able to opt-in to a gamified system for attendance monitoring. This could include high scores based on attendance data that would urge students to attend lectures more often in order to keep up with the rest of the classroom.
* **Remove cards from the equation**: The idea behind this is to relinquish the need for student cards. Students could use their smartphones to sign in to the register instead of using their student card, if they've forgotten it. This is somewhat similar to the paper aspect, as students would have one less thing to remember to carry around with them.
* **Missing regular students**: In case the university bus is delayed, a lot of students who don't usually miss lectures could be delayed from coming in to a lecture sometimes. The idea behind this is that the lecturer would get a notification on the hour, letting them know that a number of regular students have not signed in yet and that it would probably be wise to wait a few more minutes before starting the lecture, until the regular students show up.
* **Storing student data**: The main problem here would be storing data relating to students. Student data would have to be as secure as possible. Caching data could be a problem in case the information is sensitive.
* **Door scanners**: They're already there for some labs. Why not use them for monitoring attendance, instead of just opening doors?
* **E-mails to absent students**: Did a student not show up for a lecture? It could be a good idea to send e-mails to students who were expected to be in a lecture but didn't show up. This could happen right after the lecture or after a streak of non-attendance has been recorded.
* **Scientific research value**: The final point made was that the scientific research value of this project is the most important thing about it. If it becomes possible to gamify this attendance monitoring system, the results could yield funding and more resources for further evaluation and research.

~R

***Update***: Dave and I had this idea about an educational gamification engine based on what Rich said in the interview. Since Moodle is open source and widely used by many universities around the world, it could be possible to write a massive plugin that functioned as a role-playing game. Through this game, you get gear and equipment based on how well you do in your studies. Want that really nice looking sword? Get over 60% in this unit to get it. Something like that could be very interesting to work on. A possibility for the future, or even a postgraduate research idea.
