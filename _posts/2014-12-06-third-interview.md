---
layout: post
title: Third interview
---

The third (and possibly final) interview in a series of requirements gathering interviews happened last week. For this interview, I met up with Matthew Poole. I didn't manage to get to many of the pre-written questions that I intended to ask him, but I believe everything worked out at the end.

Matthew talked about getting back on track with the problem at hand and not forgetting where the focus and motivation of the project lie. He also went on to discuss the flexibility of the project as well as any possible alternatives that could end up being a better option.

Here are the main points of the interview:

* **Manual manipulation**: Matthew started off by talking about the fact that a lecturer must always have control of the register, and that they should be able to manually manipulate the records in the database.
* **Flexibility**: He went on to point out the importance of the flexibility that this project needs to have. As with above, without the flexibility of adding and removing students throughout the year, this project would fail. Similarly, it's equally important to be able to show the students who have notified the lecturer about their absence.
* **Visual feedback**: Students should only have to tap their cards. They should instantly know whether this operation has successfully taken place, so that they may leave the queue and continue.
* **Integration with third party response system**: Matthew suggested that an integration with a system like [Kahoot](https://getkahoot.com/) could be a useful alternative to see how many students have shown up to lectures.
* **Shift focus**: Matthew recommended shifting back the focus to its original issue, the reduction in the time taken for attendance tracking and monitoring. It's nice to have other features, but it shouldn't shift the focus away from the original problem.
* **Rethink assumptions**: Finally, Matthew mentioned that it's OK to assume that every student has a smartphone or a tablet with them in lectures and practicals. Therefore, it's easy to customise the project so that it better fits this audience.

~R
