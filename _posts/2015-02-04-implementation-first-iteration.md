---
layout: post
title: Implementation&#58; First Iteration
---

I didn't realise how long it's been since I posted a blog entry for my final year project. Saying that "lots of things have changed" about the project is kind of an understatement, though.

I spent most of the Christmas holidays working on the Distributed Systems and Parallel Programming unit coursework. After that was handed in, I worked on the Web Research group coursework for a couple more weeks, and then started getting back on the project again.

Here's what happened:

* The REST API Server for the project has been re-written in Node.js. The API Server is available at [atmos-api-server](https://github.com/richardsolomou/atmos-api-server).
* The Android application now holds a WebView object that provides extra functionality to the web application when it is viewed from the Android application. The hybrid application is available at [atmos-hybrid-app](https://github.com/richardsolomou/atmos-hybrid-app).
* The Web application is now the primary means of accessing the system. The web application is available at [atmos-web-app](https://github.com/richardsolomou/atmos-web-app).
* The database structure has been completely reworked and redefined.
* The API design has changed significantly to reflect the recent changes in the database structure.
* In order to keep up with the latest web development trends, I've decided to use [AngularJS](https://angularjs.org/) for the development of the front-end of the application.
* Literature review is still underway, though I have to say I've been neglecting it lately.

The only thing left to do is to e-mail my moderator and set up a meeting for the Satisfactory Progress Demonstration. That is all for now.

~R
