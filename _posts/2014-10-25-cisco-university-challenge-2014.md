---
layout: post
title: Cisco University Challenge 2014
---

On the 13th of October, a few people in the School of Computing at the University of Portsmouth received an e-mail from the BSc Computer Science course leader, Matthew Poole, on account of our very good academic performance in the second year of our degree, and to ask us if we would be interested in representing the School of Computing in the 24 hour Cisco University Challenge.

Eventually, the 5 top students from 20 UK universities were invited to participate in the first 24 hour University Challenge hosted by Cisco. I was lucky enough to be asked by my course leader to represent the Portsmouth team, with Todd Perry, Craig Wilson, Phil Hobbs, and Will Ettle.

On October 23rd, 7 AM sharp, me and the rest of the team got a taxi (booked and paid by the University) to Cisco's EMEAR Headquarters in Bedfont Lakes. We arrived there at 8:30AM and got a warm welcome by Cisco. A few talks around the [Internet of Things](http://en.wikipedia.org/wiki/Internet_of_Things) followed, and then we were given the problem statement. We decided to work on the real-time journey information problem and provide a solution for that.

We also had a tour around Cisco's security offices where we learned how Cisco deals with security alerts and warnings. We had a good laugh looking at incompetent robbers trying to break a door and getting caught a couple minutes after leaving the building. We then had a live conference talk with the Vice President of Cisco's IT Customer Strategy and Success organisation who gave us a bunch of tips on success, leadership, working in corporations, and some major life advice. Afterwards, we had a five-to-one conference talk with a guest speaker from Cisco's offices in San Jose, California.

Soon after, we got back on the project and the presentation. A litre or two of energy drink and 12 hours of non-stop coding later, and we had finished our solution and proposal. It was already 8 AM on the 24th of October. Our proposed solution to the real-time journey information problem was to have kiosks in each train station that show the status of each departure/arrival to the station, as well as a mobile application for users in which they could sign in and get personalised notifications on their specified journeys, such as when they are cancelled or delayed. Furthermore, we came up with a few other ideas such as ticketless travel using just your phone and grouping up people with similar interests or activities into the same train carriages.

The presentation to the judges went pretty smoothly, and I managed to catch a few minutes of sleep after that. We went back to the main area soon after, and the judges announced the top 4 universities in the competition, which would give one last presentation to compete for the first prize. Unfortunately, even though we had most of the technical part done already, the University of Portsmouth was not one of the top 4 universities, and therefore we didn't end up winning anything.

We did, however, have an amazing time and got a bunch of freebies, such as a Cisco bag, cake, and a &pound;20 Amazon Gift Card, plus we got to play around with the [Oculus Rift](http://www.oculus.com/)! The experience gained from this challenge was definitely something I'd give up a few more nights of sleep to experience again. We managed to get back home at 3 PM and replaced our missing hours of sleep, despite our now completely messed up sleeping patterns.

~R
