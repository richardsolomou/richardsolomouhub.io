---
layout: default
title: About
---

# About

I'm Richard, a 24-year-old front-end developer from Cyprus, with a rabid enthusiasm for **clean, reusable code** and simple, minimalistic design.

I'm currently working as a UI/UX Developer for [Feefo](http://www.feefo.com/).

## <i class="fa fa-flask"></i> Skills & Expertise

I specialise in developing business-level web applications that utilise emerging technologies and keep true to the latest standards - all while providing support for older browsers.

My **attention to detail** is only matched by my excitement to learn and work with others. I'm constantly looking for _challenging_ roles that will utilise and enhance further improvement of my skills.

## <i class="fa fa-code"></i> Technologies

My expertise lies in developing dynamic _front-end_ applications using JavaScript-driven frameworks like AngularJS.

For server-side development, I enjoy using Node.js to develop powerful REST APIs that closely follow the best practices.

I love designing responsive interfaces with regard to **user experience** and intuitive design. It's not just about creating a fancy UI - anyone can do that.

## <i class="fa fa-history"></i> History

I started making websites at the age of 8 using Netscape Composer and Microsoft FrontPage. I taught myself HTML & CSS very early on, which paved the way for everything that came later.

I quickly became very interested in PHP and took a few programming and design courses outside of school, which helped me learn and apply the best practices.

I've always been fascinated by system administration. During my teens, I started my own web hosting service for friends and family - and later on, customers - and have some experience managing and securing servers for production use.

I played around with jQuery when it came out, and through that, I discovered my love for vanilla JavaScript, as well as libraries and frameworks built around it.
